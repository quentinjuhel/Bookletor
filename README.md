# Bookletor

Bookletor is a bash script to transform pdf into booklet printable at your house, office and school. Place where there is no imposition software.

![](images/impos.jpg)

**Warning !** This tool has been only tested on linux. Is at your own risk to not see the tool working on other os, for the moment.

## Requirements

1. you must install [psutil](https://github.com/giampaolo/psutil/blob/master/INSTALL.rst).

## How it's work

- Download or clone this repository and find a nice place on youre computer to access it.
- Put the pdf of what you're want to transform into a booklet in the folder. Remember the pdf must be a multiple of 4 page to be imposed.
- Open terminal go to the bookletor folder with the folling command line: `cd /path/to/bookletor`.
- Write the instrction `./bookletor file.pdf` and push enter.
- **Option :** Booklettor come with options to let you set the booklet page number (4, 8, 16, 32), the initial page dimension and the booklet dimension :

    `./Bookletor -p 16 -o a3 -i a4 file.pdf`

    - `-p` : number of page by signature ; 
    - `-o` : the booklet dimension (ex : A3 if youre dimension page is A4);
    - `-i` : the initial page dimension ;
<br>

- Et voilà ! You've got au pdf (`file-booklet.pdf`) imposed of your publication ! 


## Thanks

I just write done instruction that are in this [tutorial](https://wiki.scribus.net/canvas/How_to_make_a_booklet).

## Licence

This software is distributed under the GNU-GPL3 license.